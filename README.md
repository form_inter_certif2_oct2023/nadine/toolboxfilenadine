
<!-- README.md is generated from README.Rmd. Please edit that file -->

# toolboxfilenadine

<!-- badges: start -->
<!-- badges: end -->

L’objectif de ce package est de vous fournir des fonctions qui donneront
de l’information sur des dossiers (contenu, taille, etc.)

## Installation

You can install the development version of toolboxfilenadine like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

Il est composé de 3 fonctions

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(toolboxfilenadine)
## basic example code
get_all_file_size(".")
#>                         file size           file_ext
#> 1              ./DESCRIPTION  524       /DESCRIPTION
#> 2                      ./dev 4096               /dev
#> 3                  ./LICENSE   44           /LICENSE
#> 4               ./LICENSE.md 1073           /LICENSE
#> 5                      ./man 4096               /man
#> 6                ./NAMESPACE  145         /NAMESPACE
#> 7                        ./R 4096                 /R
#> 8                ./README.md 1357            /README
#> 9               ./README.Rmd  817            /README
#> 10                   ./tests 4096             /tests
#> 11 ./toolboxfilenadine.Rproj  356 /toolboxfilenadine
#> 12               ./vignettes 4096         /vignettes
```
